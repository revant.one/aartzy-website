#!/bin/bash

# Check helm chart is installed or create
# reuse installed values and resets data

export CHECK_WEB=$(helm ls -q aartzy-web --tiller-namespace aartzy-website)
if [ "$CHECK_WEB" = "aartzy-web" ]
then
    echo "Updating existing aartzy-web . . ."
    helm upgrade aartzy-web \
        --tiller-namespace aartzy-website \
        --namespace aartzy-website \
        --reuse-values \
        helm-chart/aartzy-web
fi
